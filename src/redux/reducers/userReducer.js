import {
    ADD_FRIEND,
    INIT_FRIENDS,
    CLICK_FRIEND_ID,
    INIT_EXPENSE,
    ADD_EXPENSE,
    DELETE_EXPENSE,
} from "../actionType";

const initialState = {
    friends: [],
    clickFriendId: "",
    expenses: [],
};

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case INIT_FRIENDS:
            const friends = action.payload;
            return {
                ...state,
                friends,
            };
        case ADD_FRIEND:
            const friend = action.payload;
            return {
                ...state,
                friends: [...state.friends, friend],
            };
        case CLICK_FRIEND_ID:
            const friendId = action.payload;
            return {
                ...state,
                clickFriendId: friendId,
            };
        case INIT_EXPENSE:
            const expenses = action.payload;
            return {
                ...state,
                expenses,
            };
        case ADD_EXPENSE:
            const expense = action.payload;
            return {
                ...state,
                expenses: [expense, ...state.expenses],
            };
        case DELETE_EXPENSE:
            const expenseId = action.payload;
            return{
                ...state,
                expenses: state.expenses.filter((expense)=>{
                    return expense.id !== expenseId;
                })
            }
        default:
            return state;
    }
}
