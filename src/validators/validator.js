import validator from "validator";

export function nameValidator(name) {
    if (validator.isEmpty(name)) {
        return "Name can't be blank";
    } else if (name.length < 2 || name.length > 15) {
        return "Name length should be 2-15 characters";
    } else {
        return false;
    }
}

export function emailValidator(email) {
    if (validator.isEmpty(email)) {
        return "Email address can't be blank";
    } else if (validator.isEmail(email) === false) {
        return "Please enter a valid email address";
    } else {
        return false;
    }
}

export function passwordValidator(password) {
    if (validator.isEmpty(password)) {
        return "Password can't be blank";
    } else if (password.length < 8 || password.length > 16) {
        return "Password length should be 8-16 characters";
    } else if (validator.isStrongPassword(password) === false) {
        return "Password should consists of at least one uppercase letter, one lowercase letter, one number and one special character";
    } else {
        return false;
    }
}

export function descriptionValidator(description) {
    if (validator.isEmpty(description)) {
        return "Description can't be empty";
    } else if (description.length < 2 || description.length > 10) {
        return "Description length should be 2-10 characters";
    } else {
        return false;
    }
}

export function priceValidator(amount) {
    if (validator.isEmpty(amount)) {
        return "Amount can't be empty";
    } else if (Number(amount) < 1 || Number(amount) > 100000) {
        return "Amount range should be 1-100000";
    } else {
        return false;
    }
}

export function friendNameValidator(name) {
    if (validator.isEmpty(name)) {
        return "Name can't be blank";
    } else if (name.length < 2 || name.length > 10) {
        return "Name length should be 2-10 characters";
    } else {
        return false;
    }
}
