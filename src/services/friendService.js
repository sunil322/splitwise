import { myAxios } from "./helper";

export const addFriend = (friendData,token) => {
    return myAxios.post("/friends", friendData,{headers:{
        'Authorization' : `Bearer ${token}`,
        'Content-Type':'application/json'
    }});
};

export const getAllFriendsByUser = (token) => {
    return myAxios.get("/friends",{headers:{
        'Authorization' : `Bearer ${token}`,
        'Content-Type':'application/json'
    }});
};
