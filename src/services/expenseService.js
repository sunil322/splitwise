import { myAxios } from "./helper";

export const addExpense = (expense, friendId, token) => {
    return myAxios.post(`/expenses/${friendId}`, expense, {
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
};

export const deleteExpense = (friendId, token) => {
    return myAxios.delete(`/expenses/${friendId}`, {
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
};

export const getAllExpensesByFriend = (friendId, token) => {
    return myAxios.get(`/expenses/${friendId}`, {
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
};
