import { myAxios } from "./helper";

export const signup = (user) => {
    return myAxios.post("/users", user);
};

export const login = (userData) => {
    return myAxios.post("/login", userData);
};

export const getCurrentUser = (token) => {
    return myAxios.get("/current", {
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
};

