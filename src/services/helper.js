import axios from "axios";

export const BASE_URL = "https://splitwise-api.onrender.com/";

export const myAxios = axios.create({
    baseURL: BASE_URL,
});

