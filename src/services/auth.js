import { myAxios } from "./helper";

export const doLogin = (token) => {
    localStorage.setItem("token", token);
};

export const doLogout = () => {
    localStorage.removeItem("token");
};

export const revokeToken = (token) => {
    return myAxios.post("/logout",{},{
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
};