import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import { Component } from "react";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Home from "./components/Home/Home";
import { Route, Routes } from "react-router-dom";
import Login from "./components/Login/Login";
import Signup from "./components/Signup/Signup";
import UserNavbar from "./components/UserDashboard/UserNavbar";
import Dashboard from "./components/UserDashboard/Dashboard";
import { ToastContainer } from "react-toastify";
import Error from "./components/Error/Error";

class App extends Component {
    render() {
        return (
            <div>
                <ToastContainer position="bottom-center" />
                <Routes>
                    <Route
                        path="/"
                        element={
                            <>
                                <Navbar />
                                <Home />
                                <Footer />
                            </>
                        }
                    />
                    <Route
                        path="/login"
                        element={
                            <>
                                <Navbar />
                                <Login />
                                <Footer />
                            </>
                        }
                    />
                    <Route
                        path="/signup"
                        element={
                            <>
                                <Navbar />
                                <Signup />
                                <Footer />
                            </>
                        }
                    />
                    <Route
                        path="/dashboard"
                        element={
                            <>
                                <UserNavbar />
                                <Dashboard />
                            </>
                        }
                    />
                    <Route
                        path="*"
                        element={
                            <>
                                <Navbar />
                                <Error />
                                <Footer />
                            </>
                        }
                    />
                </Routes>
            </div>
        );
    }
}

export default App;
