import React, { useState } from "react";
import "./Login.css";
import { emailValidator, passwordValidator } from "../../validators/validator";
import { login } from "../../services/userService";
import { doLogin } from "../../services/auth";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

function Login() {
    const [data, setData] = useState({
        email: "",
        password: "",
        allErrors: {},
        invalidCredentials: false,
        clickOnLogin: false,
    });
    const navigate = useNavigate();

    const handleInputEvent = (event) => {
        const { name, value } = event.target;
        setData((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    const handleSubmitEvent = (event) => {
        event.preventDefault();

        let { email, password } = data;
        email = email.toLowerCase().trim();
        password = password.trim();

        const allErrors = {};

        emailValidator(email) !== false &&
            (allErrors["email"] = emailValidator(email));
        passwordValidator(password) !== false &&
            (allErrors["password"] = passwordValidator(password));

        if (Object.keys(allErrors).length === 0) {
            const userData = {
                email,
                password,
            };
            setData((prevState) => {
                return {
                    ...prevState,
                    clickOnLogin: true,
                    invalidCredentials:false
                };
            });

            login(userData)
                .then((response) => {
                    doLogin(response.data.token);
                    navigate("/dashboard");
                    setData(() => {
                        return {
                            email: "",
                            password: "",
                            allErrors: {},
                            clickOnLogin:false,
                            invalidCredentials: false,
                        };
                    });
                })
                .catch((error) => {
                    if (error.code === "ERR_NETWORK") {
                        setData((prevState) => {
                            return {
                                ...prevState,
                                allErrors: {},
                                clickOnLogin: false,
                                invalidCredentials:false
                            };
                        });
                        toast.error("Server is not responding.Try again later");
                    } else {
                        setData((prevState) => {
                            return {
                                ...prevState,
                                invalidCredentials: true,
                                allErrors: {},
                                clickOnLogin: false,
                            };
                        });
                    }
                });
        } else {
            setData((prevState) => {
                return {
                    ...prevState,
                    allErrors,
                    clickOnLogin:false,
                    invalidCredentials:false
                };
            });
        }
    };

    return (
        <div className="row justify-content-center w-100 hero m-0">
            <div className="col-xl-4 col-md-8 col-sm-10">
                {data.invalidCredentials && (
                    <div className="error-message">
                        <p>
                            Whoops! We couldn’t find an account for that email
                            address and password.
                        </p>
                    </div>
                )}
                <form className="login-form" onSubmit={handleSubmitEvent}>
                    <legend>Log in</legend>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">
                            Email address
                        </label>
                        <input
                            type="email"
                            className="form-control"
                            id="email"
                            name="email"
                            onChange={handleInputEvent}
                            value={data.email}
                        />
                    </div>
                    {data.allErrors.email !== undefined ? (
                        <p className="m-0 p-0 text-danger">
                            {data.allErrors.email}
                        </p>
                    ) : (
                        <p className="m-0 p-0 invisible">""</p>
                    )}
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">
                            Password
                        </label>
                        <input
                            type="password"
                            className="form-control"
                            id="password"
                            name="password"
                            onChange={handleInputEvent}
                            value={data.password}
                            autoComplete="off"
                        />
                    </div>
                    {data.allErrors.password !== undefined ? (
                        <p className="m-0 p-0 text-danger">
                            {data.allErrors.password}
                        </p>
                    ) : (
                        <p className="invisible m-0 p-0">""</p>
                    )}
                    <button type="submit" className="btn w-100 login-btn" disabled={data.clickOnLogin}>
                        {data.clickOnLogin ? (
                            <div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                        ) : (
                            "Login"
                        )}
                    </button>
                    <div className="text-center mt-4">
                        <p className="forgot-password">
                            Forgot your password ?
                        </p>
                        <p>or</p>
                        <button className="btn w-100 sign-google-btn">
                            <i className="fa-brands fa-google"></i> Sign in with
                            Google
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Login;
