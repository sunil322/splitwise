import React, { Component } from "react";
import "./Footer.css";
import footerImage from "../../assets/footerimg.png";

class Footer extends Component {
    render() {
        return (
            <footer className="mt-5 overflow-hidden">
                <div className="d-flex justify-content-around footer-section">
                    <section className="d-flex">
                        <div className="footer-section-1 px-4">
                            <p className="header-1">Splitwise</p>
                            <p>About</p>
                            <p>Press</p>
                            <p>Blog</p>
                            <p>Jobs</p>
                            <p>Calculators</p>
                            <p>API</p>
                        </div>
                        <div className="footer-section-2 px-4">
                            <p className="header-2">Account</p>
                            <p>Log in</p>
                            <p>Sign up</p>
                            <p>Reset Password</p>
                            <p>Settings</p>
                            <p>Splitwise Pro</p>
                            <p>Splitwise Pay</p>
                        </div>
                        <div className="px-4">
                            <p className="header-3">More</p>
                            <p>Contact us</p>
                            <p>FAQ</p>
                            <p>Terms Of Service</p>
                            <p>Privacy Policy</p>
                            <p>
                                <i className="fa-brands fa-twitter"></i>
                                <i className="fa-brands fa-square-facebook"></i>
                                <i className="fa-brands fa-instagram"></i>
                            </p>
                        </div>
                    </section>
                    <section className="d-flex flex-column align-items-end">
                        <div className="d-flex align-items-start">
                            <div className="d-flex align-items-center app-store-div">
                                <div className="px-1">
                                    <i className="fa-brands fa-google-play text-info"></i>
                                </div>
                                <div>
                                    <p>GET IT ON</p>
                                    <p>Google Play</p>
                                </div>
                            </div>
                            <div className="d-flex align-items-center app-store-div">
                                <div className="px-1">
                                    <i className="fa-brands fa-apple"></i>
                                </div>
                                <div>
                                    <p>Download on the</p>
                                    <p>App Store</p>
                                </div>
                            </div>
                        </div>
                        <div className="mx-3">
                            <p>Made with :) in Providence, RI, USA</p>
                        </div>
                    </section>
                </div>
                <div className="row">
                    <div className="col">
                        <img
                            src={footerImage}
                            className="w-100"
                            alt="footer"
                        />
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
