import React, { Component } from "react";
import logo from "../../assets/logo.png";
import "./Signup.css";
import { v4 as uuidv4 } from "uuid";
import {
    nameValidator,
    emailValidator,
    passwordValidator,
} from "../../validators/validator";
import { signup } from "../../services/userService";
import { toast } from "react-toastify";

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            allErrors: [],
            expandForm: false,
        };
    }

    handleInputEvent = (event) => {
        const { name, value } = event.target;
        this.setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    handleSubmitEvent = (event) => {
        event.preventDefault();

        let { name, email, password } = this.state;
        name = name.trim();
        email = email.toLowerCase().trim();
        password = password.trim();

        const allErrors = [];

        nameValidator(name) !== false &&
            allErrors.push({
                id: uuidv4(),
                message: nameValidator(name),
            });

        emailValidator(email) !== false &&
            allErrors.push({
                id: uuidv4(),
                message: emailValidator(email),
            });

        passwordValidator(password) !== false &&
            allErrors.push({
                id: uuidv4(),
                message: passwordValidator(password),
            });

        if (allErrors.length === 0) {
            const user = {
                username: this.state.name,
                email: this.state.email,
                password: this.state.password,
            };

            signup(user)
                .then((response) => {
                    toast.success("Successfully registered");
                    this.setState(() => {
                        return {
                            name: "",
                            email: "",
                            password: "",
                            allErrors: [],
                        };
                    });
                })
                .catch((error) => {
                    this.setState((prevState) => {
                        return {
                            ...prevState,
                            allErrors: []
                        };
                    });
                    if (error.response === undefined) {
                        toast.error("Server is not responding.Try again later");
                    } else {
                        toast.error(error.response.data.message);
                    }
                });
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    allErrors,
                };
            });
        }
    };

    render() {
        return (
            <section className="d-flex justify-content-center signup-page">
                <div className="signup-page-logo">
                    <img src={logo} alt="logo" />
                </div>
                <form className="signup-form" onSubmit={this.handleSubmitEvent}>
                    <legend>INTRODUCE YOURSELF</legend>
                    <div>
                        {this.state.allErrors.length > 0 && (
                            <div className="error-message-div">
                                The following errors occured
                                <ul>
                                    {this.state.allErrors.map((error) => {
                                        return (
                                            <li key={error.id}>
                                                {error.message}
                                            </li>
                                        );
                                    })}
                                </ul>
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="name" className="form-label name">
                            Hi there! My name is
                        </label>
                        <input
                            type="name"
                            className="form-control"
                            id="text"
                            name="name"
                            onChange={(event) =>
                                this.setState((prevState) => {
                                    return {
                                        ...prevState,
                                        expandForm: true,
                                        [event.target.name]: event.target.value,
                                    };
                                })
                            }
                            value={this.state.name}
                        />
                    </div>
                    {this.state.expandForm && (
                        <>
                            <div className="mb-3">
                                <label htmlFor="email" className="form-label">
                                    Here’s my <b>email address:</b>
                                </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    id="email"
                                    name="email"
                                    onChange={this.handleInputEvent}
                                    value={this.state.email}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="password"
                                    className="form-label"
                                >
                                    And here’s my <b>password:</b>
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    onChange={this.handleInputEvent}
                                    value={this.state.password}
                                    autoComplete="off"
                                />
                            </div>
                        </>
                    )}
                    <div className="d-flex justify-content-between align-items-center button-div">
                        <button type="submit" className="btn signup-btn">
                            Sign me up!
                        </button>
                        <span className="or-text">or</span>
                        <span>
                            <button className="google-signup-btn">
                                <i className="fa-brands fa-google"></i> Sign up
                                with Google
                            </button>
                        </span>
                    </div>
                    <div className="terms-div">
                        <p className="terms-text mt-3">
                            By signing up, you accept the Splitwise Terms of
                            Service.
                        </p>
                        <p>
                            {this.state.expandForm && (
                                <>
                                    <span className="currency-text">
                                        Don't use USD for currency?
                                    </span>
                                    <span className="terms-text">
                                        Click here.
                                    </span>
                                </>
                            )}
                        </p>
                    </div>
                </form>
            </section>
        );
    }
}

export default Signup;
