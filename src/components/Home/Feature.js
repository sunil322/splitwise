import React, { Component } from "react";

class Feature extends Component {
    render() {
        return (
            <div className="container-fluid">
                <h1 className="text-center feature-header">The whole nine yards</h1>
                <div className="row justify-content-center feature-text">
                    <div className="col-xl-3 col-md-4 col-sm-10">
                        <p><i className="fa-solid fa-diamond text-info"></i> Add groups and friends</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Split expenses, record debts</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Equal or unequal splits</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Split by % or shares</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Calculate total balances</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Suggested repayments</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Simplify debts</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Recurring expenses</p>
                    </div>
                    <div className="col-xl-3 col-md-4 col-sm-10">
                        <p><i className="fa-solid fa-diamond text-info"></i> Offline mode</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Cloud sync</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Spending totals</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Categorize expenses</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Easy CSV exports</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> 7+ languages</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> 100+ currencies</p>
                        <p><i className="fa-solid fa-diamond text-info"></i> Payment integrations</p>
                    </div>
                    <div className="col-xl-3 col-md-4 col-sm-10">
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> A totally ad-free experience</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Currency conversion</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Receipt scanning</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Itemization</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Charts and graphs</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Expense search</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Save default splits</p>
                        <p><i className="fa-sharp fa-solid fa-gem text-purple"></i> Early access to new features</p>
                    </div>
                </div>
                <div className="d-flex justify-content-center mt-4">
                    <div className="p-3"><i className="fa-solid fa-diamond text-info"></i> Core features</div>
                    <div className="p-3"><i className="fa-sharp fa-solid fa-gem text-purple "></i> Pro features</div>
                </div>
            </div>
        );
    }
}

export default Feature;
