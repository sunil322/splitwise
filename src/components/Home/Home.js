import React, { Component } from "react";
import image1 from "../../assets/image1.png";
import image2 from "../../assets/image2.png";
import image3 from "../../assets/image3.png";
import image4 from "../../assets/image4.png";
import image5 from "../../assets/image5.png";
import Hero from "./Hero";
import "./Home.css";
import planeIcon from "../../assets/plane.png";
import Feature from "./Feature";
import Reviews from "./Reviews";
import { Link } from "react-router-dom";

class Home extends Component {
    constructor(props) {
        super(props);
        this.text = [
            "on trips",
            "with housemates",
            "with your partner",
            "with anyone",
        ];
        this.state = {
            showText: this.text[0],
        };
    }
    componentDidMount() {
        this.interval = setInterval(() => {
            switch (this.state.showText) {
                case this.text[0]:
                    this.setState({
                        showText: this.text[1],
                    });
                    break;
                case this.text[1]:
                    this.setState({
                        showText: this.text[2],
                    });
                    break;
                case this.text[2]:
                    this.setState({
                        showText: this.text[3],
                    });
                    break;
                default:
                    this.setState({
                        showText: this.text[0],
                    });
            }
        }, 4000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="d-flex justify-content-center hero row-1-hero">
                        <div className="text-div">
                            <h1>Less stress when sharing expenses</h1>
                            <h1
                                style={
                                    this.state.showText === this.text[2]
                                        ? { color: "rgb(166,0,47)" }
                                        : this.state.showText === this.text[1]
                                        ? { color: 'rgb(134,86,205)' }
                                        : { color: "rgb(28, 194, 159)" }
                                }
                            >
                                {this.state.showText}
                            </h1>
                            <p>
                                <span>
                                    <i className="fa-solid fa-plane-departure text-info"></i>
                                </span>
                                <span>
                                    <i className="fa-solid fa-house-chimney text-purple"></i>
                                </span>
                                <span>
                                    <i className="fa-solid fa-heart text-danger"></i>
                                </span>
                                <span>
                                    <i className="fa-solid fa-star-of-life"></i>
                                </span>
                            </p>
                            <p>
                                Keep housemates, trips, groups, friends, and
                                family.
                            </p>
                            <Link to='/signup'>
                                <button
                                    style={
                                        this.state.showText === this.text[2]
                                            ? { backgroundColor: "rgb(166,0,47)" }
                                            : this.state.showText === this.text[1]
                                            ? { backgroundColor: "rgb(134,86,205)" }
                                            : { backgroundColor:"rgb(28, 194, 159)"}
                                    }
                                >
                                    Sign up
                                </button>
                            </Link>
                            <p className="app-store-desc">
                                Free for <i className="fa-brands fa-apple"></i>{" "}
                                iphone, <i className="fa-brands fa-android"></i>{" "}
                                Android and Web.
                            </p>
                        </div>
                        <div className="img-fluid img-div">
                            <img src={planeIcon} alt="plane-icon" />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <Hero
                        image={image1}
                        bgColor="rgb(60,64,68)"
                        text={{
                            header: "Track balances",
                            description:
                                "Keep track of shared expenses, balances, and who owes who.",
                        }}
                    />
                    <Hero
                        image={image2}
                        bgColor="rgb(28, 194, 159)"
                        text={{
                            header: "Organize expenses",
                            description:
                                "Split expenses with any group: trips, housemates, friends, and family.",
                        }}
                    />
                </div>
                <div className="row">
                    <Hero
                        image={image3}
                        bgColor="rgb(238,113,66)"
                        text={{
                            header: "Add expenses easily",
                            description:
                                "Quickly add expenses on the go before you forget who paid.",
                        }}
                    />
                    <Hero
                        image={image4}
                        bgColor="rgb(60,64,68)"
                        text={{
                            header: "Pay friends back",
                            description:
                                "Settle up with a friend and record any cash or online payment.",
                        }}
                    />
                </div>
                <div className="row">
                    <div
                        className="d-flex justify-content-around hero row-4-hero"
                        style={{ backgroundColor: "rgb(132,87,200" }}
                    >
                        <div>
                            <h3>Get even more with PRO</h3>
                            <p>
                                Get even more organized with receipt scanning,
                                charts and graphs, currency conversion, and
                                more!
                            </p>
                            <button>Sign up</button>
                        </div>
                        <img src={image5} alt="hero" />
                    </div>
                </div>
                <div className="row">
                    <Feature />
                </div>
                <div className="row">
                    <Reviews />
                </div>
            </div>
        );
    }
}

export default Home;
