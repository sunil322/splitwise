import React,{Component} from "react";

class ReviewCard extends Component{
    render(){
        return(
            <div className="card pt-4 px-3">
                <p className="title">{this.props.title}</p>
                <p className="fst-italic mt-4" style={{fontStyle:"italic"}}>{this.props.author}</p>
            </div>
        );
    }
}

export default ReviewCard;