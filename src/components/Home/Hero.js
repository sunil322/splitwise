import React, { Component } from "react";

class Hero extends Component {
    render() {
        return (
            <div
                className="col-xl-6 col-md-12 col-sm-12 d-flex flex-column justify-content-end align-items-center hero"
                style={{ backgroundColor: this.props.bgColor }}
            >
                <h3 className="text-white pt-5">{this.props.text.header}</h3>
                <div className="text-white text-center mt-2 description-div">
                    <p>{this.props.text.description}</p>
                </div>
                <img
                    className="img-fluid mt-3"
                    src={this.props.image}
                    alt="hero"
                />
            </div>
        );
    }
}

export default Hero;
