import React, { Component } from "react";
import ReviewCard from "./ReviewCard";

class Reviews extends Component {
    render() {
        return (
            <div>
                <div className="d-flex flex-wrap justify-content-center mt-5 review-card">
                    <ReviewCard
                        title='"Fundamental" for tracking finances. As good as WhatsApp for containing awkwardness.'
                        author="Financial Times"
                    />
                    <ReviewCard
                        title="Life hack for group trips. Amazing tool to use when travelling with friends! Make life so easy!!"
                        author="Ahah S, iOS"
                    />
                    <ReviewCard
                        title="Makes it easy to split everything from your dinner to bill rent."
                        author="NY Times"
                    />
                    <ReviewCard
                        title="So amazing to have this app manage balances and help keep money out of relationships. Love it!"
                        author="Haseena C, Android"
                    />
                    <ReviewCard
                        title="I never fights with roommates over bills because of this genius expense-splitting app."
                        author="Business Insider"
                    />
                    <ReviewCard
                        title="I use it everyday. I use it for trips, roommates, loans. I love splitwise."
                        author="Trickseyus, iOS"
                    />
                </div>
            </div>
        );
    }
}

export default Reviews;
