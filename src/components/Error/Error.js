import React, { Component } from "react";
import errorLogo from "../../assets/error.png";
import { Link } from "react-router-dom";

class Error extends Component {
    render() {
        return (
            <div className="text-center my-5">
                <img src={errorLogo} alt="error" className="img-fluid" />
                <h1 className="my-2 text-danger">404 Error</h1>
                <p>The Page you are looking for doesn't exist</p>
                <Link to="/">
                    <button className="btn text-white my-2" style={{backgroundColor:'rgb(28, 194, 159)'}}>
                        Back to Home
                    </button>
                </Link>
            </div>
        );
    }
}

export default Error;
