import React, { Component } from "react";
import logo from "../../assets/splitwiselogo.png";
import {
    descriptionValidator,
    priceValidator,
} from "../../validators/validator";
import { v4 as uuidv4 } from "uuid";
import { connect } from "react-redux";
import { addExpense } from "../../services/expenseService";
import { toast } from "react-toastify";
import Expenses from "./Expenses";
import { ADD_EXPENSE } from "../../redux/actionType";

class MiddleComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            description: "",
            amount: "",
            allErrors: [],
            expenseList: [],
            clickOnAddExpense: false,
        };
    }

    handleInputEvent = (event) => {
        const { name, value } = event.target;
        this.setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    handleSubmitEvent = (event) => {
        event.preventDefault();
        const { description, amount } = this.state;

        const allErrors = [];

        let descValidationData = descriptionValidator(description.trim());
        let priceValidationData = priceValidator(amount);

        descValidationData !== false &&
            allErrors.push({
                id: uuidv4(),
                message: descValidationData,
            });

        priceValidationData !== false &&
            allErrors.push({
                id: uuidv4,
                message: priceValidationData,
            });

        if (allErrors.length === 0) {
            const expense = {
                description,
                amount,
            };

            addExpense(expense, this.props.clickFriendId,localStorage.getItem('token'))
                .then((response) => {
                    toast.success("Expense added");
                    this.props.addExpenseHandler(response.data);
                    this.setState(() => {
                        return {
                            description: "",
                            amount: "",
                            allErrors: [],
                            clickOnAddExpense: false,
                        };
                    });
                })
                .catch((error) => {
                    toast.error("Error occured in add expense, Try again later");
                });
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    allErrors,
                };
            });
        }
    };

    currentClickFriend = () => {
        let friend = this.props.friends.find((friend) => {
            return friend.id === this.props.clickFriendId;
        });
        if (friend === undefined) {
            return false;
        } else {
            return friend;
        }
    };

    render() {
        return (
            <div className="middle-component">
                <div className="d-flex justify-content-between align-items-center py-2 px-1 header">
                    <div className="d-flex align-items-center">
                        <img src={logo} alt="logo" />
                        <h2>
                            {this.props.clickFriendId !== "" &&
                            this.currentClickFriend() !== false
                                ? this.currentClickFriend().name
                                : "Dashboard"}
                        </h2>
                    </div>
                    {this.props.clickFriendId !== "" && (
                        <div className="text-center">
                            <button className="btn btn-expense m-1"
                                onClick={() =>
                                    this.setState({ clickOnAddExpense: true })
                                }
                            >
                                Add an expense
                            </button>
                            <button className="btn btn-settle m-1">
                                Settle up
                            </button>
                        </div>
                    )}
                </div>
                <hr className="m-0" />
                <div className="add-expense-div">
                    {this.state.clickOnAddExpense && (
                        <div className="add-expense-container">
                            <div className="header">
                                <h4 className="text-center">Add an expense</h4>
                                {this.state.allErrors.length > 0 && (
                                    <div className="add-expense-error-div">
                                        <ul>
                                        {this.state.allErrors.map((error) => {
                                            return (
                                                <li
                                                    className="m-0 py-1"
                                                    key={error.id}
                                                >
                                                    {error.message}
                                                </li>
                                            );
                                        })}
                                        </ul>
                                    </div>
                                )}
                            </div>
                            <form
                                className="text-center p-3"
                                onSubmit={this.handleSubmitEvent}
                            >
                                <div>
                                    <input
                                        type="text"
                                        placeholder="Enter a description"
                                        name="description"
                                        value={this.state.description}
                                        onChange={this.handleInputEvent}
                                    />
                                </div>
                                <div>
                                    <input
                                        type="number"
                                        placeholder="0"
                                        name="amount"
                                        value={this.state.amount}
                                        onChange={this.handleInputEvent}
                                    />
                                </div>
                                <div>
                                    <button
                                        className="btn bg-danger text-white mx-1"
                                        type="submit"
                                        onClick={() =>
                                            this.setState(() => {
                                                return {
                                                    desccription: "",
                                                    amount: "",
                                                    allErrors: [],
                                                    clickOnAddExpense: false,
                                                };
                                            })
                                        }
                                    >
                                        cancel
                                    </button>
                                    <button className="btn bg-green-light text-white save-btn">
                                        save
                                    </button>
                                </div>
                            </form>
                        </div>
                    )}
                </div>
                <div className="header month">
                    <p>{new Date().toDateString()}</p>
                </div>
                {this.props.clickFriendId !== "" && (
                    <Expenses
                        friendId={this.props.clickFriendId}
                        friendName={this.currentClickFriend().name}
                    />
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const friends = state.userReducer.friends;
    const clickFriendId = state.userReducer.clickFriendId;
    return {
        friends,
        clickFriendId,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        addExpenseHandler: (expense) =>
            dispatch({
                type: ADD_EXPENSE,
                payload: expense,
            }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MiddleComponent);
