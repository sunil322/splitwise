import React, { Component } from "react";
import "./UserNavbar.css";
import logo from "../../assets/splitwiselogo.png";
import { Link } from "react-router-dom";
import { getCurrentUser } from "../../services/userService";
import { doLogout, revokeToken } from "../../services/auth";
import { toast } from "react-toastify";

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginUserName: "",
        };
    }

    componentDidMount() {
        localStorage.getItem("token") &&
            getCurrentUser(localStorage.getItem("token"))
                .then((response) => {
                    this.setState({
                        loginUserName: response.data.username,
                    });
                })
                .catch((error) => {
                });
    }

    handleSignout = () => {
        revokeToken(localStorage.getItem("token"))
            .then(() => {
                doLogout();
                window.location.href = "/login";
            })
            .catch((error) => {
                toast.error("something went wrong");
            });
    };

    render() {
        return (
            <>
                {this.state.loginUserName && (
                    <nav className="d-flex justify-content-around align-items-center user-navbar">
                        <Link to="/dashboard">
                            <div className="d-flex align-items-center nav-logo">
                                <img src={logo} alt="logo" />
                                <span className="text-white">Splitwise</span>
                            </div>
                        </Link>
                        <div className="dropdown">
                            <button
                                className="btn dropdown-toggle text-white"
                                type="button"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                            >
                                <span>
                                    <img
                                        src={logo}
                                        alt="logo"
                                        className="user-profile-logo"
                                    />
                                </span>
                                <span className="p-1">
                                    {this.state.loginUserName}
                                </span>
                            </button>
                            <ul className="dropdown-menu p-2 text-center">
                                <li className="py-1">Your account</li>
                                <li className="py-1">create a group</li>
                                <li className="py-1">Fairness calculators</li>
                                <li className="py-1">Contact support</li>
                                <li
                                    className="py-1"
                                    onClick={this.handleSignout}
                                >
                                    Logout
                                </li>
                            </ul>
                        </div>
                    </nav>
                )}
            </>
        );
    }
}

export default Navbar;
