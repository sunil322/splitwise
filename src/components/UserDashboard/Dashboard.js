import React, { useEffect, useState } from "react";
import "./Dashboard.css";
import LeftSideComponent from "./LeftSideComponent";
import MiddleComponent from "./MiddleComponent";
import { getCurrentUser } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { getAllFriendsByUser } from "../../services/friendService";
import { connect } from "react-redux";
import { INIT_FRIENDS } from "../../redux/actionType";
import { ScaleLoader } from "react-spinners";
import RightSideComponent from "./RightSideComponent";

function Dashboard(props) {
    const API_LOADING = "LOADING";
    const API_LOADED = "LOADED";
    const API_ERROR = "ERROR";

    const navigate = useNavigate();

    const [validUser, setValidUser] = useState(false);

    const [status, setStatus] = useState(API_LOADING);

    useEffect(() => {
        getCurrentUser(localStorage.getItem("token"))
            .then(() => {
                setValidUser(true);
            })
            .catch(() => {
                setValidUser(false);
                navigate("/login");
            });

        localStorage.getItem("token") !== null &&
            getAllFriendsByUser(localStorage.getItem("token"))
                .then((response) => {
                    props.initFriedsList(response.data);
                    setStatus(API_LOADED);
                })
                .catch((error) => {
                    setStatus(API_ERROR);
                });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <div className="d-flex justify-content-center align-items-center" style={{height: status !== API_LOADED ? '100vh' :'0vh'}}>
                <ScaleLoader
                    color={"rgb(28, 194, 159)"}
                    loading={status === API_LOADING}
                    size={25}
                    aria-label="Loading Spinner"
                    data-testid="loader"
                />
            </div>
            {validUser && (
                <div className="row justify-content-center w-100 m-0">
                    <div className="col-xxl-2 col-xl-3 col-lg-2 col-md-3">
                        <LeftSideComponent/>
                    </div>
                    <div className="col-xxl-4 col-xl-5 col-lg-6 col-md-7">
                        <MiddleComponent />
                    </div>
                    <div className="col-xxl-2 col-xl-3 col-lg-2 col-md-2">
                        <RightSideComponent/>
                    </div>
                </div>
            )}
        </>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        initFriedsList: (friends) =>
            dispatch({
                type: INIT_FRIENDS,
                payload: friends,
            }),
    };
};

export default connect(null, mapDispatchToProps)(Dashboard);
