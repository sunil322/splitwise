import React, { Component } from "react";
import { deleteExpense } from "../../services/expenseService";
import { connect } from "react-redux";
import { DELETE_EXPENSE } from "../../redux/actionType";
import Swal from "sweetalert2";

class Expense extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clickOnExpense: false,
        };
    }

    removeExpense = (id) => {
        Swal.fire({
            title: "Are you sure you want to delete?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "confirm",
        }).then((result) => {
            if(result.isConfirmed){
                deleteExpense(id,localStorage.getItem("token"))
                .then(() => {
                    this.props.deleteExpenseHandler(id);
                    Swal.fire("Deleted!", "Expense has been deleted.", "success");
                })
                .catch(() => {
                    Swal.fire("Error","something went wrong","error");
                });
            }
        });
    };

    render() {
        const { id, description, amount, created_at } = this.props.expense;

        const createdDate = new Date(created_at);

        const date = createdDate.getDate();
        const month = createdDate.toLocaleString("default", { month: "long" });

        return (
            <>
                <div
                    className="d-flex justify-content-between expense-list"
                    onMouseEnter={() => this.setState({ clickOnExpense: true })}
                    onMouseLeave={() =>
                        this.setState({ clickOnExpense: false })
                    }
                >
                    <div className="left-div d-flex align-items-center">
                        <div className="px-3">
                            <p className="top-text">{month}</p>
                            <p className="bottom-text">{date}</p>
                        </div>
                        <img
                            src="https://s3.amazonaws.com/splitwise/uploads/category/icon/square_v2/uncategorized/general@2x.png"
                            alt="note-img"
                            className="notebook-img mx-2"
                        />
                        <div>
                            <p>{description}</p>
                        </div>
                    </div>
                    <div className="right-div px-3 d-flex">
                        <section className="text-end px-3">
                            <p className="top-text">You paid</p>
                            <p className="bottom-text">${amount}</p>
                        </section>
                        <section>
                            <p className="top-text">
                                You lent {this.props.name}
                            </p>
                            <p className="bottom-text text-green-light">
                                ${amount / 2}
                            </p>
                        </section>
                        {this.state.clickOnExpense ? (
                            <div className="d-flex align-items-center mx-2">
                                <i
                                    className="fa-solid fa-xmark text-danger"
                                    onClick={() => this.removeExpense(id)}
                                ></i>
                            </div>
                        ) : (
                            <div className="mx-2 invisible">
                                <i className="fa-solid fa-xmark"></i>
                            </div>
                        )}
                    </div>
                </div>
                <hr />
            </>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteExpenseHandler: (expenseId) => {
            dispatch({
                type: DELETE_EXPENSE,
                payload: expenseId,
            });
        },
    };
};

export default connect(null, mapDispatchToProps)(Expense);
