import React, { Component } from "react";
import { connect } from "react-redux";

class RightSideComponent extends Component {
    render() {
        const totalExpenses = this.props.expenses.reduce(
            (totalExpense, expense) => {
                return totalExpense + expense.amount;
            },0);

        const friend = this.props.friends.find((friend)=>{
            return friend.id === this.props.clickFriendId;
        });

        return (
            <>
                <div className="d-flex align-items-center text-secondary gap-2 mt-2">
                    <i className="fa-solid fa-bars"></i>
                    <i className="fa-regular fa-calendar-days"></i>
                    <i className="fa-solid fa-chart-simple"></i>
                    <i className="fa-solid fa-gear"></i>
                </div>
                <div className="my-4">
                    <p className="text-secondary">YOUR BALANCE</p>
                    {totalExpenses === 0 ? (
                        <p className="text-secondary">You are all settled up</p>
                    ) : (
                        <>
                            <p className="m-0 p-0 text-green-light">
                                {friend.name} owes you
                            </p>
                            <h3 className="text-green-light">
                                ${totalExpenses / 2}
                            </h3>
                        </>
                    )}
                </div>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    const expenses = state.userReducer.expenses;
    const friends = state.userReducer.friends;
    const clickFriendId = state.userReducer.clickFriendId;
    return {
        expenses,
        friends,
        clickFriendId
    };
};

export default connect(mapStateToProps, null)(RightSideComponent);
