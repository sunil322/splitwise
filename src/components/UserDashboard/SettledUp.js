import React, { Component } from "react";
import settleImage  from "../../assets/correct.png";

class SettledUp extends Component {
    render() {
        return (
            <div className="text-center mt-5">
                <img src={settleImage} alt="settle-png" className="w-25"/>
                <p className="my-3 text-secondary">You and {this.props.name} are all settled up</p>
            </div>
        );
    }
}

export default SettledUp;
