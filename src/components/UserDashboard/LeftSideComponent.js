import React, { Component } from "react";
import logo from "../../assets/logo.png";
import { connect } from "react-redux";
import {
    ADD_FRIEND,
    CLICK_FRIEND_ID,
} from "../../redux/actionType";
import { friendNameValidator } from "../../validators/validator";
import { addFriend } from "../../services/friendService";
import { toast } from "react-toastify";

class LeftSideComponent extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            toggleAddBox: false,
            error: ""
        };
    }

    handleInput = (event) => {
        const { name, value } = event.target;
        this.setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const friendName = this.state.name.trim();
        let error = "";

        friendNameValidator(friendName) !== false &&
            (error = friendNameValidator(friendName));

        if (error === "") {
            addFriend({ name: friendName },localStorage.getItem('token'))
                .then((response) => {
                    this.props.addFriend(response.data);
                    toast.success("Friend Added");
                })
                .catch((error) => {
                    toast.error("Error occured in add friend, Try again later");
                });

            this.setState((prevState) => {
                return {
                    ...prevState,
                    name: "",
                    error: "",
                };
            });
        } else {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    error,
                };
            });
        }
    };

    clickFriendId = (id) => {
        this.props.clickFriendId(id);
    };

    render() {
        return (
            <section className="left-component">
                <div className="mb-2">
                    <img src={logo} alt="logo"></img>
                    <span>Dashboard</span>
                </div>
                <div className="mb-3">
                    <i className="fa-solid fa-flag"></i>
                    <span>Recent activity</span>
                </div>
                <div className="mb-3">
                    <i className="fa fa-list"></i>
                    <span>All expenses</span>
                </div>
                <div className="d-flex justify-content-between align-items-center list-header">
                    <p className="m-0">FRIENDS</p>
                    <p
                        className="m-0 addToggleBox"
                        onClick={() =>
                            this.setState({
                                toggleAddBox: !this.state.toggleAddBox,
                            })
                        }
                    >
                        {this.state.toggleAddBox ? (
                            <i className="fa-solid fa-minus"></i>
                        ) : (
                            <i className="fa-solid fa-plus"></i>
                        )}
                        <span>add</span>
                    </p>
                </div>
                {this.state.toggleAddBox && (
                    <div>
                        <form
                            className="d-flex justify-content-between align-items-center"
                            onSubmit={this.handleSubmit}
                        >
                            <input
                                type="text"
                                className="form-control"
                                name="name"
                                onChange={this.handleInput}
                                value={this.state.name}
                            />
                            <button className="btn border">Add</button>
                        </form>
                        <p className="text-danger">{this.state.error}</p>
                    </div>
                )}

                {this.props.friends.map((friend) => {
                    return (
                        <div
                            className="py-1 my-1 friend-list"
                            key={friend.id}
                            onClick={() => this.clickFriendId(friend.id)}
                        >
                            <i className="fa fa-user px-2"></i>
                            <span className="m-0">{friend.name}</span>
                        </div>
                    );
                })}
            </section>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addFriend: (friend) =>
            dispatch({
                type: ADD_FRIEND,
                payload: friend,
            }),
        clickFriendId: (friendId) =>
            dispatch({
                type: CLICK_FRIEND_ID,
                payload: friendId,
            }),
    };
};

const mapStateToProps = (state) => {
    const friends = state.userReducer.friends;
    return {
        friends,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftSideComponent);
