import React, { Component } from "react";
import { getAllExpensesByFriend } from "../../services/expenseService";
import Expense from "./Expense";
import { connect } from "react-redux";
import { INIT_EXPENSE } from "../../redux/actionType";
import SettledUp from "./SettledUp";
import { ScaleLoader } from "react-spinners";
import alertPng from "../../assets/alerts.png";

class Expenses extends Component {
    API_STATES = {
        LOADING: "LOADING",
        LOADED: "LOADED",
        ERROR: "ERROR",
    };
    constructor(props) {
        super(props);

        this.state = {
            expenseList: [],
            status: this.API_STATES.LOADING,
        };
    }
    fetchData() {
        this.setState(
            (prevState) => {
                return {
                    ...prevState,
                    status: this.API_STATES.LOADING,
                };
            },
            () => {
                getAllExpensesByFriend(this.props.friendId,localStorage.getItem('token'))
                    .then((response) => {
                        this.props.initExpenses(response.data);
                        this.setState((prevState) => {
                            return {
                                ...prevState,
                                expenseList: response.data,
                                status: this.API_STATES.LOADED,
                            };
                        });
                    })
                    .catch((error) => {
                        this.setState((prevState) => {
                            return {
                                ...prevState,
                                status: this.API_STATES.ERROR,
                            };
                        });
                    });
            }
        );
    }

    componentDidMount() {
        this.fetchData();
    }
    componentDidUpdate(prevProps) {
        if (prevProps.friendId !== this.props.friendId) {
            this.fetchData();
        }
    }

    render() {
        return (
            <>
                <div className="text-center">
                    <ScaleLoader
                        color={"rgb(28, 194, 159)"}
                        loading={this.state.status === this.API_STATES.LOADING}
                        size={25}
                        aria-label="Loading Spinner"
                        data-testid="loader"
                    />
                </div>
                {this.state.status === this.API_STATES.LOADED &&
                    this.props.expenses.length !== 0 &&
                    this.props.expenses.map((expense) => {
                        return (
                            <Expense
                                key={expense.id}
                                expense={expense}
                                name={this.props.friendName}
                            />
                        );
                    })}
                    {
                        (this.state.status === this.API_STATES.LOADED && this.props.expenses.length === 0) &&
                        <SettledUp name ={this.props.friendName}/>
                    }
                    {
                        this.state.status === this.API_STATES.ERROR && 
                        <div className="text-center my-5">
                            <img src={alertPng} alt='alert' className="w-25"/>
                            <p>Internal server error</p>
                        </div>
                    }
            </>
        );
    }
}

const mapStateToProps = (state) => {
    const expenses = state.userReducer.expenses;
    return {
        expenses,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initExpenses: (expenses) =>
            dispatch({
                type: INIT_EXPENSE,
                payload: expenses,
            }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Expenses);
