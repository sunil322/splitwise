import React, { Component } from "react";
import "./Navbar.css";
import logo from "../../assets/logo.png";
import { Link } from "react-router-dom";

class Navbar extends Component {
    render() {
        return (
            <div className="navbar">
                <div className="logo-div">
                    <Link to="/">
                        <img src={logo} alt="logo" className="logo" />
                        <span>Splitwise</span>
                    </Link>
                </div>
                <div className="nav-menu-div">
                    <Link to="/login">
                        <button className="login-btn">Log in</button>
                    </Link>
                    <Link to="/signup">
                        <button className="signup-btn">Sign up</button>
                    </Link>
                </div>
            </div>
        );
    }
}

export default Navbar;
